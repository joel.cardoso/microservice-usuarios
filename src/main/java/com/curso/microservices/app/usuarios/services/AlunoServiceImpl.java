package com.curso.microservices.app.usuarios.services;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.curso.microservices.app.usuarios.models.repository.AlunoRepository;
import com.curso.microservices.commons.alunos.models.entity.Aluno;
import com.curso.microservices.commons.services.CommonServiceImpl;

@Service
public class AlunoServiceImpl extends CommonServiceImpl<Aluno, AlunoRepository> implements AlunoService{

	@Transactional(readOnly = true)
	@Override
	public List<Aluno> findByNomeOrApelido(String term) {
		return repository.findByNomeOrApelido(term);
	}

}
