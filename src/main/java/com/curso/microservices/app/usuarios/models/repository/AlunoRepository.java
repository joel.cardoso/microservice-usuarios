package com.curso.microservices.app.usuarios.models.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.curso.microservices.commons.alunos.models.entity.Aluno;

@Repository
public interface AlunoRepository extends PagingAndSortingRepository<Aluno, Long> {
	
	@Query("select a from Aluno a where a.nome like %?1% or a.apelido like %?1%")
	public List<Aluno> findByNomeOrApelido(String term);
}
