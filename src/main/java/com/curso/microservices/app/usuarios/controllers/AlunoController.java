package com.curso.microservices.app.usuarios.controllers;

import java.io.IOException;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.curso.microservices.app.usuarios.services.AlunoService;
import com.curso.microservices.commons.alunos.models.entity.Aluno;
import com.curso.microservices.commons.controllers.CommonController;

@RestController
public class AlunoController extends CommonController<Aluno, AlunoService>{
	
	@GetMapping("/uploads/img/{id}")
	public ResponseEntity<?> getFoto(@PathVariable Long id) {
		Optional<Aluno> optionalAluno = service.findById(id);
		
		if (optionalAluno.isEmpty() || optionalAluno.get().getFoto() == null) {
			return ResponseEntity.notFound().build();
		}
		
		Resource image = new ByteArrayResource(optionalAluno.get().getFoto());
		
		return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(image);
	}
	
	@PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> save(@Valid Aluno aluno, BindingResult result, @RequestParam MultipartFile file) throws IOException {
		if (!file.isEmpty()) {
			aluno.setFoto(file.getBytes());
		}
		return super.save(aluno, result);
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> update(@Valid Aluno aluno, BindingResult result, @PathVariable Long id, @RequestParam MultipartFile file) throws IOException {
		
		if (result.hasErrors()) {
			return this.validate(result);
		}
		
		Optional<Aluno> optionalAluno = service.findById(id);
		
		if (optionalAluno.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		
		Aluno alunoDB = optionalAluno.get();
		alunoDB.setNome(aluno.getNome());
		alunoDB.setApelido(aluno.getApelido());
		alunoDB.setEmail(aluno.getEmail());
		
		if (!file.isEmpty()) {
			alunoDB.setFoto(file.getBytes());
		}
		
		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(alunoDB));
	}
	
	@GetMapping("/filtrar/{term}")
	public ResponseEntity<?> filter(@PathVariable String term) {
		return ResponseEntity.ok(service.findByNomeOrApelido(term));
	}
}
