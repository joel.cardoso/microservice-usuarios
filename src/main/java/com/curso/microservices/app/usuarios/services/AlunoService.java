package com.curso.microservices.app.usuarios.services;

import java.util.List;

import com.curso.microservices.commons.alunos.models.entity.Aluno;
import com.curso.microservices.commons.services.CommonService;

public interface AlunoService extends CommonService<Aluno>{
	
	public List<Aluno> findByNomeOrApelido(String term);
}
